from random import randint

from flask import render_template, request
from form import SubmitForm
from run import app


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    submit_form = SubmitForm()
    if request.method == 'GET':
        return render_template('index.html', form=submit_form)
    if request.method == 'POST':
        if submit_form.validate_on_submit():
            omikuji_list = ['大凶', '凶', '末吉', '小吉', '吉', '大吉']
            omikuji_result = omikuji_list[randint(0, len(omikuji_list) - 1)]
            return render_template('result.html', result=omikuji_result)
        else:
            return render_template('index.html', form=submit_form)